import struct
import sys
from dataclasses import dataclass


class MpbData:
    def __init__(self):
        self.channels = []
        self.data_size: int = 0
        self.metrsPerPoint = 0.0001


def parseMpbFull(file):
    ret = MpbData()
    f = open(file, "rb")
    f.seek(20, 0)
    channelsCount = int.from_bytes(f.read(4), byteorder='little', signed=True)
    f.seek(24, 0)

    ret.data_size = int.from_bytes(f.read(4), byteorder='little', signed=True)
    f.seek(12, 0)
    multiplier = struct.unpack('d', f.read(8))[0]
    multiplier = 1.0/multiplier
    f.seek(28, 0)
    position = 28

    for i in range(0, channelsCount):
        ret.channels.append([])

    for j in range(0, ret.data_size):

        for i in range(0, channelsCount):
            ret.channels[i].append(int.from_bytes(
                f.read(2), byteorder='little', signed=True) * multiplier)

        position += 48
        f.seek(position)

    # position -= 48
    # f.seek(position)

    position += 8
    f.seek(position)

    ret.metrsPerPoint = struct.unpack('d', f.read(8))[0]

    f.close()
    return ret


if __name__ == "__main__":
    # data = parseMpbFull(
    #     "fol/D73-10_СОП19-С_20А. Поперечные пропилы  и круговая риска.mpb")

    print(f"Output file: {sys.argv[2]}")
    print(f"Input file: {sys.argv[1]}")

    data = parseMpbFull(sys.argv[1])

    print(f"Size of data: {data.data_size}")
    print(f"Channels: {len(data.channels)}")

    fileOut = open(sys.argv[2], "w")

    for j in range(0, len(data.channels)):
        fileOut.write(str(data.channels[j]) + "\n")

    fileOut.close()
    print("Done!")
